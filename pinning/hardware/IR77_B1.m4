dnl
dnl aumeIR-B1.m4
dnl
dnl Pin Configuration for 'aumeIR-B1'.  Edit it to fit your needs.
dnl

/* rc5 support */
pin(RC5_SEND, PD4)
RC5_USE_INT(1)

pin(CHANNEL1, PC3)
pin(CHANNEL2, PC2)
pin(CHANNEL3, PC1)
pin(CHANNEL4, PC0)
pin(CHANNEL5, PB0)
pin(CHANNEL6, PD7)
pin(CHANNEL7, PD6)
pin(CHANNEL8, PD5)
